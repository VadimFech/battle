// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYPROJECT_HumanPlayer_generated_h
#error "HumanPlayer.generated.h already included, missing '#pragma once' in HumanPlayer.h"
#endif
#define MYPROJECT_HumanPlayer_generated_h

#define MyProject_Source_MyProject_HumanPlayer_h_22_SPARSE_DATA
#define MyProject_Source_MyProject_HumanPlayer_h_22_RPC_WRAPPERS
#define MyProject_Source_MyProject_HumanPlayer_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define MyProject_Source_MyProject_HumanPlayer_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAHumanPlayer(); \
	friend struct Z_Construct_UClass_AHumanPlayer_Statics; \
public: \
	DECLARE_CLASS(AHumanPlayer, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(AHumanPlayer)


#define MyProject_Source_MyProject_HumanPlayer_h_22_INCLASS \
private: \
	static void StaticRegisterNativesAHumanPlayer(); \
	friend struct Z_Construct_UClass_AHumanPlayer_Statics; \
public: \
	DECLARE_CLASS(AHumanPlayer, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(AHumanPlayer)


#define MyProject_Source_MyProject_HumanPlayer_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AHumanPlayer(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AHumanPlayer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AHumanPlayer); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AHumanPlayer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AHumanPlayer(AHumanPlayer&&); \
	NO_API AHumanPlayer(const AHumanPlayer&); \
public:


#define MyProject_Source_MyProject_HumanPlayer_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AHumanPlayer(AHumanPlayer&&); \
	NO_API AHumanPlayer(const AHumanPlayer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AHumanPlayer); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AHumanPlayer); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AHumanPlayer)


#define MyProject_Source_MyProject_HumanPlayer_h_22_PRIVATE_PROPERTY_OFFSET
#define MyProject_Source_MyProject_HumanPlayer_h_19_PROLOG
#define MyProject_Source_MyProject_HumanPlayer_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject_Source_MyProject_HumanPlayer_h_22_PRIVATE_PROPERTY_OFFSET \
	MyProject_Source_MyProject_HumanPlayer_h_22_SPARSE_DATA \
	MyProject_Source_MyProject_HumanPlayer_h_22_RPC_WRAPPERS \
	MyProject_Source_MyProject_HumanPlayer_h_22_INCLASS \
	MyProject_Source_MyProject_HumanPlayer_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyProject_Source_MyProject_HumanPlayer_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject_Source_MyProject_HumanPlayer_h_22_PRIVATE_PROPERTY_OFFSET \
	MyProject_Source_MyProject_HumanPlayer_h_22_SPARSE_DATA \
	MyProject_Source_MyProject_HumanPlayer_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	MyProject_Source_MyProject_HumanPlayer_h_22_INCLASS_NO_PURE_DECLS \
	MyProject_Source_MyProject_HumanPlayer_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYPROJECT_API UClass* StaticClass<class AHumanPlayer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyProject_Source_MyProject_HumanPlayer_h


#define FOREACH_ENUM_EMOVEMENTDIRECTION(op) \
	op(EMovementDirection::UP) \
	op(EMovementDirection::DOWN) \
	op(EMovementDirection::STOP) \
	op(EMovementDirection::RIGHT) \
	op(EMovementDirection::LEFT) 

enum class EMovementDirection;
template<> MYPROJECT_API UEnum* StaticEnum<EMovementDirection>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS

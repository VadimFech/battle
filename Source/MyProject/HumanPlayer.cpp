// Fill out your copyright notice in the Description page of Project Settings.


#include "HumanPlayer.h"

// Sets default values
AHumanPlayer::AHumanPlayer()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MovementSpeed = 10.f;
	RotationSpeed = 0.f;
}

// Called when the game starts or when spawned
void AHumanPlayer::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AHumanPlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move(DeltaTime);
}

void AHumanPlayer::Move(float DeltaTime)
{
	FQuat QuatRotation = FQuat(FRotator(0, RotationSpeed, 0));
	FVector MovementVector(ForceInitToZero);
	float MovementSpeedDelta = MovementSpeed * DeltaTime;

	switch (MoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += MovementSpeedDelta;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= MovementSpeedDelta;
		break;
	case EMovementDirection::STOP:
		MovementVector.X = 0;
		RotationSpeed = 0.f;
		break;
	case EMovementDirection::RIGHT:
		RotationSpeed = -1.f;
		break;
	case EMovementDirection::LEFT:
		RotationSpeed = 1.f;
		break;
	}

	AddActorLocalRotation(QuatRotation, false, 0, ETeleportType::None);
	AddActorWorldOffset(MovementVector);
}


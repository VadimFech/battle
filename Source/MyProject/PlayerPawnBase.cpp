// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "HumanPlayer.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	
	SetActorRotation(FRotator(-90, 0, 0));
	CreateHumanPlayerActor();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveStraight", this, &APlayerPawnBase::HandlePlayerMoveStraightInput);
	PlayerInputComponent->BindAxis("RotateRight", this, &APlayerPawnBase::HandlePlayerRotateRightInput);
}

void APlayerPawnBase::CreateHumanPlayerActor()
{
	HumanPlayerActor = GetWorld()->SpawnActor<AHumanPlayer>(HumanPlayerActorClass, FTransform());
}

void APlayerPawnBase::HandlePlayerMoveStraightInput(float value)
{
	if (IsValid(HumanPlayerActor))
	{
		if (value > 0)
		{
			HumanPlayerActor->MoveDirection = EMovementDirection::UP;
		}
		else if (value < 0)
		{
			HumanPlayerActor->MoveDirection = EMovementDirection::DOWN;
		}
		else
		{
			HumanPlayerActor->MoveDirection = EMovementDirection::STOP;
		}
	}
}

void APlayerPawnBase::HandlePlayerRotateRightInput(float value)
{
	if (IsValid(HumanPlayerActor))
	{
		if (value > 0)
		{
			HumanPlayerActor->MoveDirection = EMovementDirection::RIGHT;
		}
		else if (value < 0)
		{
			HumanPlayerActor->MoveDirection = EMovementDirection::LEFT;
		}
		else
		{
			HumanPlayerActor->MoveDirection = EMovementDirection::STOP;
		}
	}
}

